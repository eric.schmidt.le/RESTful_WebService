import Vue from 'vue';
import {store} from "./store.jsx";
import {authRepository} from "./storage/repository.jsx";

window.app = new Vue({
    el: '#root',
    store: store,
    data: {
        sidebar: null
    },

    beforeMount: function () {
        let self = this;

        // We wait till phonegap apis are ready
        document.addEventListener("deviceready", function () {
            console.log("device ready");
            // now check if there is a auth token already stored
            authRepository.getToken().then(function (token) {
                // we have a token
                self.api.authenticateWithToken(token);
                self.$store.dispatch('loadInitialData');
            }, function (e) {
                // token expired or error while reading file
                console.log("No token available. Reason:");
                console.log(e);
            }).then(function () {
                // A tiny delay so that the changes to api sync and the login-screen doesn't flicker
                setTimeout(function () {
                    self.hideSplashScreen();
                }, 100);
            });
        }, false);
    },

    mounted: function () {
        // initialize sidebar
        this.sidebar = $('.ui.sidebar');

        this.sidebar.sidebar(
            {
                context: $('#app-container .pushable')
            }
        ).sidebar('attach events', '.toggle-button');
    },

    methods: {
        makeActive: function (item) {
            this.$store.state.currentView = item;
            this.sidebar.sidebar('toggle');
            switch (item) {
                case 'menus':
                    this.$store.state.currentTitle = 'Wochenmenü';
                    break;
                case 'customer':
                    this.$store.state.currentTitle = 'Mein Konto';
                    break;
                case 'dish':
                    this.$store.state.currentTitle = 'Unsere Gerichte';
                    break;
                case 'service':
                    this.$store.state.currentTitle = 'Service';
                    break;
                default:
                    this.$store.state.currentTitle = '';
            }
        },
        hideSplashScreen: function () {
            if (navigator) {
                navigator.splashscreen.hide();
            }
        },
        logout: function () {
            this.api.logout();
        },
    },

    computed: {
        currentView: function () {
            return this.$store.state.currentView;
        },
        currentTitle: function () {
            return this.$store.state.currentTitle;
        },
        api: function () {
            return this.$store.getters.api;
        }
    },

    components: {
        // element components
        menus: function (resolve) {
            require(['./components/views/menus'], resolve)
        },
        dish: function (resolve) {
            require(['./components/views/dish'], resolve)
        },
        loginScreen: function (resolve) {
            require(['./components/views/login-screen'], resolve)
        },
        mealDetail: function (resolve) {
            require(['./components/views/meal-detail'], resolve)
        },
        customer: function (resolve) {
            require(['./components/views/customer-data'], resolve)
        },
        dishPicker: function (resolve) {
            require(['./components/views/dish-picker'], resolve)
        },
        service: function (resolve) {
            require(['./components/views/service'], resolve)
        },
    }
});

